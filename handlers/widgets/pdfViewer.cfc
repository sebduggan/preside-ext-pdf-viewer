component {

	property name="assetManagerService" inject="assetManagerService";

	private function index( event, rc, prc, args={} ) {
		args.clientId = getSystemSetting( "pdfViewer", "client_id" );
		if ( !len( args.clientId ) ) {
			return translateResource( "widgets.pdfViewer:clientid.not.configured" );
		}

		var embedMode    = args.embed_mode ?: "";
		var asset        = assetManagerService.getAsset( args.pdf_asset );
		args.assetId     = asset.id;
		args.pdfUrl      = event.buildLink( assetId=asset.id );
		args.pdfFilename = asset.file_name & "." & asset.asset_type;
		args.pdfOptions  = _getOptions( args=args );

		if ( embedMode == "SIZED_CONTAINER" ) {
			var dimensions   = assetManagerService.getAssetDerivative(
				  assetId        = asset.id
				, derivativeName = "adminThumbnail"
				, versionId      = asset.active_version
				, configHash     = assetManagerService.getDerivativeConfigHash( assetManagerService.getDerivativeConfig( asset.id ) )
				, selectFields   = [ "width", "height" ]
			);
			args.width  = dimensions.width;
			args.height = dimensions.height;
		}

		event.include( "/js/frontend/specific/pdfViewer/" );

		return renderView( view="widgets/pdfViewer/_#embedMode#", args=args );
	}

	private function placeholder( event, rc, prc, args={} ) {
		return renderView( view="widgets/pdfViewer/placeholder", args=args );
	}


	private string function _getOptions( required struct args ) {
		var options   = {};
		var embedMode = UCase( args.embed_mode ?: "" );

		options[ "embedMode" ] = embedMode;

		switch( embedMode ) {
			case "SIZED_CONTAINER":
				options[ "showDownloadPDF" ]  = isTrue( args.show_download      ?: "" );
				options[ "showPrintPDF" ]     = isTrue( args.show_print         ?: "" );
				options[ "showFullScreen" ]   = isTrue( args.show_full_screen   ?: "" );
				options[ "dockPageControls" ] = isTrue( args.dock_page_controls ?: "" );
				break;
			case "IN_LINE":
				options[ "showDownloadPDF" ] = isTrue( args.show_download      ?: "" );
				options[ "showPrintPDF" ]    = isTrue( args.show_print         ?: "" );
				break;
			case "LIGHT_BOX":
				options[ "showDownloadPDF" ]  = isTrue( args.show_download      ?: "" );
				options[ "showPrintPDF" ]     = isTrue( args.show_print         ?: "" );
				break;
		}

		return serializeJSON( options );
	}
}
