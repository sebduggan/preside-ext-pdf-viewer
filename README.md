# PDF Viewer for Preside

This extension allows you to embed PDF files in web pages using the [Adobe PDF Embed API](https://www.adobe.io/apis/documentcloud/dcsdk/pdf-embed.html).

The PDF Embed API allows cross-platform display of PDFs in web pages with no installation required.

## Pre-requisites

In order to use the Adobe PDF Embed API, you need to obtain (free) credentials for the Adobe Document Services API:

https://www.adobe.io/apis/documentcloud/dcsdk/gettingstarted.html

Enter the provided Client ID in the PDF Viewer system settings panel, and you're ready to go!

## The widget

Use the widget to choose a PDF file from the Asset Manager, and one of three available embed modes. Each mode has a few extra options available to control the PDF display interface.

### Lightbox

Lightbox mode displays a rendered thumbnail of the PDF on the webpage; when clicked, this will load the PDF in a lightbox overlay, filling the whole viewport.

The advantage of this method is that the PDF content is only loaded when the user chooses to view it (the Adobe Viewer produces a lot more data to transfer than just the size of the original PDF).

### Sized container

The sized container renders the PDF in a container which fills all the available width, and whose height is governed by the aspect ratio of the PDF content; the container fits a single page of the PDF at a time.

### In-line

In-line rendering displays the PDF in a container which fills all the available width. However, it is displayed as one long, unpaged piece of content.