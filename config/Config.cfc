component {

	/**
	 * This extension Config.cfc has been scaffolded by the Preside
	 * Scaffolding service.
	 *
	 * Override or append to core Preside/Coldbox settings here.
	 *
	 */
	public void function configure( required struct config ) {
		// core settings that will effect Preside
		var settings            = arguments.config.settings            ?: {};

		// other ColdBox settings
		var coldbox             = arguments.config.coldbox             ?: {};
		var i18n                = arguments.config.i18n                ?: {};
		var interceptors        = arguments.config.interceptors        ?: {};
		var interceptorSettings = arguments.config.interceptorSettings ?: {};
		var cacheBox            = arguments.config.cacheBox            ?: {};
		var wirebox             = arguments.config.wirebox             ?: {};
		var logbox              = arguments.config.logbox              ?: {};
		var environments        = arguments.config.environments        ?: {};


		interceptors.prepend( { class="app.extensions.preside-ext-pdf-viewer.interceptors.pdfViewerInterceptor", properties={} } );

		settings.enum.pdfEmbedModes = [ "LIGHT_BOX", "SIZED_CONTAINER", "IN_LINE" ];

		settings.assetManager.derivatives.pdfViewerThumbnail = {
			  permissions = "inherit"
			, transformations = [
				  { method="pdfPreview" , args={ page=1 }, inputfiletype="pdf", outputfiletype="jpg" }
				, { method="shrinkToFit", args={ width=500, height=500, quality="highQuality" } }
			  ]
		};
	}
}