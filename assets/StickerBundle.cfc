/**
 * Sticker bundle configuration file. See: http://sticker.readthedocs.org/
 */

component output=false {

	public void function configure( bundle ) output=false {
		bundle.addAssets(
			  directory   = "/js/"
			, match       = function( path ){ return ReFindNoCase( "_[0-9a-f]{8}\..*?\.min.js$", arguments.path ); }
			, idGenerator = function( path ) {
				return ListDeleteAt( path, ListLen( path, "/" ), "/" ) & "/";
			}
		);

		bundle.addAsset( id="adobePdfViewerSdk", url="https://documentcloud.adobe.com/view-sdk/viewer.js" );
		bundle.asset( "/js/frontend/specific/pdfViewer/" ).dependsOn( "adobePdfViewerSdk" );
	}

}