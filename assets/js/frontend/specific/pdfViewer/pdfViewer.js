( function() {
	const pdfViewers = document.querySelectorAll( ".pdf-viewer" );

	for( let i=0; i<pdfViewers.length; i++ ) {
		let viewer      = pdfViewers[ i ]
		  , viewerId    = viewer.getAttribute( "id" )
		  , clientId    = viewer.getAttribute( "data-client-id" )
		  , pdfUrl      = viewer.getAttribute( "data-pdf-url" )
		  , pdfFilename = viewer.getAttribute( "data-pdf-filename" )
		  , pdfOptions  = JSON.parse( viewer.getAttribute( "data-pdf-options" ) )
		  , showPdf     = function( e ){
		  		e.preventDefault();

		  		let adobeDCView = new AdobeDC.View( { clientId: clientId, divId: viewerId } );
				adobeDCView.previewFile( {
					content: {
						location: {
							url: pdfUrl
						}
					},
					metaData: {
						fileName: pdfFilename
					}
				}, pdfOptions );
		  };

		if ( document.addEventListener ) {
			if ( pdfOptions.embedMode == "LIGHT_BOX" ) {
				let launchButton = document.querySelector( "#"+viewerId+"-lightbox" );
				document.addEventListener( "adobe_dc_view_sdk.ready", function(){
					launchButton.addEventListener( "click", showPdf );
				} );
			} else {
				document.addEventListener( "adobe_dc_view_sdk.ready", showPdf );
			}
		}
	}

}() );
