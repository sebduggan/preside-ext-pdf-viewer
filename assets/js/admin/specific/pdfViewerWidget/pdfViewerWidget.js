( function( $ ) {

	var $form        = $( "#widget-pdfViewer" )
	  , $embedMode   = $( "[name=embed_mode]", $form )
	  , validOptions = {
	  	    "SIZED_CONTAINER" : [ "show_download", "show_print", "show_full_screen", "dock_page_controls" ]
	  	  , "IN_LINE"         : [ "show_download", "show_print" ]
	  	  , "LIGHT_BOX"       : [ "show_download", "show_print" ]
	  };

	$embedMode.on( "change", function(){
		var selectedMode  = $embedMode.filter( ":checked" ).val()
		  , optionsToShow = validOptions[ selectedMode ] || [];

		$( "#fieldset-options .form-group" ).hide();

		for( var i=0; i<optionsToShow.length; i++ ) {
			$( "[name=" + optionsToShow[ i ] + "]" ).closest( ".form-group" ).show();
		}
	} ).trigger( "change" );

} )( presideJQuery );
