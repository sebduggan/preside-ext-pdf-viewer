component extends="coldbox.system.Interceptor" {

// PUBLIC
	public void function configure() {}

	public void function preRenderForm( event, interceptData ) {
		var formName = interceptData.formName ?: "";

		if ( formName == "widgets.pdfViewer" ) {
			event.include( "/js/admin/specific/pdfViewerWidget/" );
		}
	}

}
