# Changelog

## v1.1.1

* Fix scoping issue with multiple PDFs on same page

## v1.1.0

* Update to use newer viewer script for better UX

## v1.0.3

* Fix aspect ratio padding fallback on sized container

## v1.0.2

* Fix sized container rendering if calculated dimensions are invalid

## v1.0.1

* Move lightbox link text to i18n

## v1.0.0

* Initial public release
