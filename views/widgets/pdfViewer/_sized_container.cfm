<cfscript>
	assetId     = args.assetId     ?: "";
	clientId    = args.clientId    ?: "";
	pdfUrl      = args.pdfUrl      ?: "";
	pdfFilename = args.pdfFilename ?: "";
	pdfOptions  = args.pdfOptions  ?: "";
	width       = args.width       ?: "";
	height      = args.height      ?: "";
	aspectRatio = Val( height ) && Val( width ) ? width / height : 1;
	instanceId  = LCase( hash( createUUID() ) );
</cfscript>

<cfoutput>
	<style>
		##pdf-viewer-#instanceId# {
			aspect-ratio : #aspectRatio#;
			width        : 100%;
			max-width    : 100%;
		}

		@supports not ( aspect-ratio: auto ) {
			##pdf-viewer-#instanceId#-container {
				position       : relative;
				height         : 0;
				padding-bottom : #(1/aspectRatio)*100#%;
			}
			##pdf-viewer-#instanceId# {
				position : absolute;
				height   : 100%;
				top      : 0;
				left     : 0;
		}
	</style>
	<div id="pdf-viewer-#instanceId#-container">
		<div class="pdf-viewer" id="pdf-viewer-#instanceId#"
			data-client-id="#clientId#"
			data-pdf-url="#pdfUrl#"
			data-pdf-filename="#pdfFilename#"
			data-pdf-options="#ESAPIEncode( "html_attr", pdfOptions )#"></div>
	</div>
</cfoutput>