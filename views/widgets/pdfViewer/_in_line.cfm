<cfscript>
	assetId     = args.assetId     ?: "";
	clientId    = args.clientId    ?: "";
	pdfUrl      = args.pdfUrl      ?: "";
	pdfFilename = args.pdfFilename ?: "";
	pdfOptions  = args.pdfOptions  ?: "";
	instanceId  = LCase( hash( createUUID() ) );
</cfscript>

<cfoutput>
	<div class="pdf-viewer" id="pdf-viewer-#instanceId#"
		data-client-id="#clientId#"
		data-pdf-url="#pdfUrl#"
		data-pdf-filename="#pdfFilename#"
		data-pdf-options="#ESAPIEncode( "html_attr", pdfOptions )#"></div>
</cfoutput>