<cfscript>
	assetId     = args.assetId     ?: "";
	clientId    = args.clientId    ?: "";
	pdfUrl      = args.pdfUrl      ?: "";
	pdfFilename = args.pdfFilename ?: "";
	pdfOptions  = args.pdfOptions  ?: "";
	instanceId  = LCase( hash( createUUID() ) );
</cfscript>

<style>
	.pdf-viewer-launcher-preview {
		display  : inline-block;
		margin   : 1em 0;
		position : relative;
	}
	.pdf-viewer-launcher-preview img {
		display : block;
	}
	.pdf-viewer-launcher-hint {
		display     : block;
		opacity     : 0;
		position    : absolute;
		bottom      : 0;
		left        : 0;
		right       : 0;
		height      : 3em;
		line-height : 3em;
		background  : rgba( 0,0,0,.7 );
		color       : white;
		font-weight : bold;
		text-align  : center;
		transition  : opacity .25s;
	}
	.pdf-viewer-launcher-preview a:hover .pdf-viewer-launcher-hint {
		opacity : 1;
	}
</style>

<cfoutput>
	<div class="pdf-viewer" id="pdf-viewer-#instanceId#"
		data-client-id="#clientId#"
		data-pdf-url="#pdfUrl#"
		data-pdf-filename="#pdfFilename#"
		data-pdf-options="#ESAPIEncode( "html_attr", pdfOptions )#"></div>

	<div class="pdf-viewer-launcher-preview">
		<a href="#pdfUrl#" id="pdf-viewer-#instanceId#-lightbox">
			<img src="#event.buildLink( assetId=assetId, derivative="pdfViewerThumbnail" )#">
			<span class="pdf-viewer-launcher-hint">#translateResource( "widgets.pdfViewer:lightbox.view.hint" )#</span>
		</a>
	</div>
</cfoutput>