<cfparam name="args.pdf_asset"  default="" />
<cfparam name="args.embed_mode" default="" />

<cfoutput>
	<strong>#translateResource( uri='widgets.pdfViewer:placeholder.title' )#</strong><br>
	#translateResource( "enum.pdfEmbedModes:#args.embed_mode#.short_label" )#<br>
	<img src="#event.buildLink( assetId=args.pdf_asset, derivative="adminThumbnail" )#" style="margin-top:0.5em;">
</cfoutput>